<img src="../img/thumbnail.png" align="center" title="Murrengan network"/>


[murrengan.ru](https://murrengan.ru/)

<a href="../../../"><img src="../img/russian_federation.png" height="25" width="30" title="Русский"></a>

### This repository contains the code of the social network Murrengan
Development is done via fork and pull requests.

### Current features

* Client registration. Email confirmation and other social networks
* Creating text cards
* Add and crop photos
* Tree comments
* Like/dislike rating system

### Development requires

* [Python 3.7](https://www.python.org/downloads/release/python-369/)
* [Node 12](https://nodejs.org/)
* [Django 3](https://www.djangoproject.com/) 
* [Vue](https://vuejs.org) 

### Installation
```bash
# make sure that the virtual environment with python 3.7 is activated

git clone https://gitlab.com/malyshka_morgo/murr.git # copy the project locally
pip install -r requirements.txt  # installing python dependencies
python manage.py migrate # database migration (preparation) 
python manage.py prepare_stand # create an administrator and test Murren
npm i # installing node dependencies

```

### Launch
```bash
# first console
python manage.py runserver
# second console
npm run serve
```

### Docker
```bash
# windows - for entrypoint.sh put line separator LF instead of CRLF (can be changed in pycharm)

chmod +x entrypoint.sh
docker-compose -f docker-compose-prod.yml up --build
```

###### For https, get origin_ca_rsa_root. pem and private_origin_ca_ecc_root.pem certificates from cloudflare.com and place them in . / nginx

# 🌟Support project🌟 
[donationalerts](http://bit.do/eWnnm)

# ❤

## Contacts

[Telegram](https://t.me/MurrenganChat)

[youtube](https://youtube.com/murrengan/)

[vk](https://vk.com/murrengan)

[murrengan](https://www.murrengan.ru/)
